const report = require("multiple-cucumber-html-reporter");
const fs = require("fs-extra");
const path = require("path");

const cucumberJsonDir = path.resolve(process.cwd(), "cypress/cucumber-json");
const cucumberReportFileMap = {};
const cucumberReportMap = {};
const jsonIndentLevel = 2;
const htmlReportDir = path.resolve(process.cwd(), "cypress/cucumber-json");
const screenshotsDir = path.resolve(process.cwd(), "cypress/screenshots");

function verifyOrCreateDirectory(directoryPath) {
  if (!fs.existsSync(directoryPath)) {
    fs.mkdirSync(directoryPath);
  }
}

function ObtenerNavegadorYSO() {
  const ExtraerNavegador = path.resolve(__dirname, 'cypress/fixtures/read-write/navegadorAndSo.json');
  const fsync = require("fs");
  const datos = fsync.readFileSync(ExtraerNavegador, 'utf-8')
  const navegador = JSON.parse(datos);
  generateReport(navegador.navegador, navegador.sistemaOperativo);
}

function getCucumberReportMaps() {
  verifyOrCreateDirectory(cucumberJsonDir)
  const files = fs.readdirSync(cucumberJsonDir).filter((file) => {
    return file.indexOf(".json") > -1;
  });
  files.forEach((file) => {
    const json = JSON.parse(fs.readFileSync(path.join(cucumberJsonDir, file)));
    if (!json[0]) {
      return;
    }
    const [feature] = json[0].uri.split("/").reverse();
    cucumberReportFileMap[feature] = file;
    cucumberReportMap[feature] = json;
  });
}

function findAndUpdateReportWithScreenshot(feature, screenshot) {
  const filename = screenshot.replace(/^.*[\\\/]/, "");
  const featureSelected = cucumberReportMap[feature][0];

  let myScenarios = [];
  cucumberReportMap[feature][0].elements.forEach((item) => {
    let fullFileName = featureSelected.name + " -- " + item.name;
    if (filename.includes(fullFileName)) {
      myScenarios.push(item);
    }
  });
  if (!myScenarios) {
    return;
  }
  let foundFailedStep = false;
  myScenarios.forEach((myScenario) => {
    if (foundFailedStep) {
      return;
    }
    let myStep;
    if (screenshot.includes("(failed)")) {
      myStep = myScenario.steps.find(
        (step) => step.result.status === "failed"
      );
    } else {
      myStep = myScenario.steps.find(
        (step) => step.result.status === "passed"
      );
    }
    if (!myStep) {
      return;
    }
    const data = fs.readFileSync(path.resolve(screenshot));
    if (data) {
      const base64Image = Buffer.from(data, "binary").toString("base64");
      if (!myStep.embeddings) {
        myStep.embeddings = [];
        myStep.embeddings.push({
          data: base64Image,
          mime_type: "image/png",
          name: myStep.name,
        });
        foundFailedStep = true;
      }
    }
  });
  fs.writeFileSync(
    path.join(cucumberJsonDir, cucumberReportFileMap[feature]),
    JSON.stringify(cucumberReportMap[feature], null, jsonIndentLevel)
  );
}

function addScreenshots() {
  if (!fs.existsSync(screenshotsDir)) {
    fs.mkdirSync(screenshotsDir)
    return
  }
  const prependPathSegment = (pathSegment) => (location) =>
    path.join(pathSegment, location);

  const readdirPreserveRelativePath = (location) =>
    fs.readdirSync(location).map(prependPathSegment(location));
  const readdirRecursive = (location) =>
    readdirPreserveRelativePath(location).reduce(
      (result, currentValue) =>
        fs.statSync(currentValue).isDirectory()
          ? result.concat(readdirRecursive(currentValue))
          : result.concat(currentValue),
      []
    );
  const screenshots = readdirRecursive(path.resolve(screenshotsDir)).filter(
    (file) => {
      return file.indexOf(".png") > -1;
    }
  );
  const featuresList = Array.from(
    new Set(screenshots.map((x) =>x.match(/(\w[-._]*)*.feature/g)[0]))
  );
  featuresList.forEach((feature) => {
    screenshots.forEach((screenshot) => {
      findAndUpdateReportWithScreenshot(feature, screenshot)
    });
  });
}

function generateReport(navegador, sistemaOperativo) {
  if (!fs.existsSync(cucumberJsonDir)) {
    console.warn("REPORT CANNOT BE CREATED!");
  } else {
    report.generate({
      jsonDir: cucumberJsonDir,
      reportPath: htmlReportDir,
      displayDuration: true,
      useCDN: true,
      pageTitle: "Reporte de cypress",
      reportName: `${path.basename(__dirname)} - ${new Date().toLocaleString()}`,
      metadata: {
        app: {
          name: path.basename(__dirname),
          version: "1",
        },
        browser: {
          name: `${navegador}`,
        },
        device: "EMULATOR",
        platform: {
          name: `${sistemaOperativo}`,
        },
      },
      customData: {
        title: "Run info",
        data: [
          { label: "Project", value: path.basename(__dirname) },
          { label: "Release", value: "1" },
          {
            label: "Execution Start Time",
            value: `${new Date().toLocaleString()}`,
          },
          {
            label: "Execution End Time",
            value: new Date().toLocaleString(),
          },
        ],
      },
    });
  }
}

function generateGeneralJson() {
  const cukemerge = require('cucumber-json-merge');
  const files = cukemerge.listJsonFiles(cucumberJsonDir, false);
  if (Array.isArray(files)) {
    const merged = cukemerge.mergeFiles(files);
    const reportFinalFolder = `${cucumberJsonDir}/json`
    verifyOrCreateDirectory(reportFinalFolder)
    fs.writeFileSync(path.resolve(reportFinalFolder, 'cucumber.json'), merged, 'utf-8');
  }
}

function doReportActivities() {
  getCucumberReportMaps();
  addScreenshots();
  ObtenerNavegadorYSO();
  generateGeneralJson();
}

module.exports = { ObtenerNavegadorYSO, generateGeneralJson, doReportActivities };