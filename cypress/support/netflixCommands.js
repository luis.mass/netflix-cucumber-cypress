const Ajv = require("ajv");
const ajv = new Ajv();

/**Maximiza pantalla en función del tipo de dispositivo escogido 
 *  Args:
       -[string] device: dispositivo que se quiere emular.
*/
Cypress.Commands.add('maximizeWindows', (device) => {
    let width, height;
    
    cy.clearCookies();
  
    switch (device) {
      case 'desktop':
        width = 1920;
        height = 1080;
        break;
      case 'tablet':
        width = 768;
        height = 1024;
        break;
      case 'mobile':
        width = 375;
        height = 667;
        break;
      default:
        width = 1024;
        height = 768;
    }
  
    cy.viewport(width, height);
  });



  /** Guarda una variable para ser reutilizada en steps diferentes.
   * Args:
       -[string] variableName: Nombre de la variable a guardar.
       -[*] value: Valor a almacenar.
  */
  Cypress.Commands.add('saveVariable', (variableName, value) => {
    cy.wrap(value).as(variableName);
  });



  /** Valida que una página inlcuya en su título un texto predefinido y lo imprime. 
   * Args:
       - [string] textToValidate: Texto a validar en el título.
  */
  Cypress.Commands.add("titleContainsText", (textToValidate) => {
    cy.title().should("include", textToValidate).then((title) => {
        cy.log(`Título de la página: ${title}`);
      });
  });



  /** Valida que una página inlcuya en su url un texto predefinido y lo imprime 
   * Args:
       - [string] textToValidate: Texto a validar en la url.
  */
  Cypress.Commands.add("urlContainsText", (textToValidate) => {
    cy.url().should('include', textToValidate).then((url) => {
        cy.log(`Url de la página: ${url}`)
    });
  });

  Cypress.Commands.add('changeLanguageIfNeeded', () => {
    cy.url().should('include', 'https://www.netflix.com/').then((url) => {
      if(url.includes('en')) {
        const newUrl = url.replace('en', 'es');
        cy.visit(newUrl); // Redirigir a la nueva URL
      }
    });
  });




  /**
 * Realiza la validación de un esquema JSON en base a un conjunto de datos.
 * Args:
 *  - [object] schema - El esquema JSON a validar.
 *  - [object] data - Los datos que se validarán con el esquema.
 */
  Cypress.Commands.add('validateSchema', (schema, data) => {
    const validate = ajv.compile(schema);
    const valid = validate(data);
    if (!valid) {
      console.log(`************ ${ajv.errorsText(validate.errors)} ******************`) 
    }
    expect(valid, 'Response JSON schema validation').to.be.true;
  });


    /**
   * Genera un correo aleatorio
   * Args:
   *  - [int] length - número de caracteres antes del @
   *  - [string] domain - dominio a concatenar después del @
   */
  Cypress.Commands.add('generateRandomEmail', (length, domain) => {
    const allowedChars = "abcdefghijklmnopqrstuvwxyz0123456789";
    let randomString = "";
    for (let i = 0; i < length; i++) {
      const randomIndex = Math.floor(Math.random() * allowedChars.length);
      randomString += allowedChars.charAt(randomIndex);
    }
    const randomEmail = `${randomString}@${domain}`;
    return cy.wrap(randomEmail);
  });

  