import { Given, Then, When } from "cypress-cucumber-preprocessor/steps";

const landingPage = require("../../fixtures/page_objects/LandingPage.json")
const loginPage = require("../../fixtures/page_objects/LoginPage.json")
const signUpPage = require("../../fixtures/page_objects/SignUpPage.json")
const geoLocationSchema = require('../../fixtures/schemas/schema_geoLocation.json')

Given("I have the base url: {string}", (url) => {
    //Configuramos en el escenario background la url base para todas las pruebas.
    Cypress.config("baseUrl", url); //la mejor práctica es configurar esta variable baseUrl desde el cypress.config como una variable de entorno, no desde mi feature como lo estoy haciendo.
})

When("I maximize the screen in device: {string}", (deviceUser) => {
    cy.visit("/"); //Navegar a la url base
    cy.changeLanguageIfNeeded(); //En ocaciones se carga la página en inglés, esto depende también del país de origen y si se usan vpns
    cy.maximizeWindows(deviceUser); //maximizamos en función del dispositivo
    cy.waitFor(cy.get(landingPage.labelTitleAssert).should('be.visible').and('have.text','Películas y series ilimitadas y mucho más'));
});

Then("Print and validate the title page", () => {
    //Validación del título, dependiendo del país cambia este label, sin embargo, todos los países en español tienen en común estos textos
    cy.titleContainsText('Netflix'); 
    cy.titleContainsText('Ve series online, ve películas online');
});

Then("Print and validate the url", () => {
    //Validación de url
    cy.urlContainsText('https://www.netflix.com/');
    cy.clearCookies(); //Para abrir luego otro dispositivo es necesario borrar cookies
});

When("I navigate to the Website and count the page load time", () => {
    //Capturamos el tiempo transcurrido desde que se abre el navegador hasta que se cargue el html y lo almacenamos en responseTime
    const startTime = Date.now();
    cy.visit("/");
    cy.get(landingPage.labelBodyAssert).should("not.be.empty").then(() => {
      const endTime = Date.now();
      const loadTimeMilliseconds = endTime - startTime;    
      cy.saveVariable('responseTime', loadTimeMilliseconds);
    });
});

Then("I validate that the page timeout is less than {int} seconds", (MaxTime) => {
    //Capturamos el tiempo almacenado en responseTime y lo comparamos.
    cy.get('@responseTime').then((timeCalculated) => {
        const maxTimeInMilliseconds = MaxTime*1000
        cy.log(`Tiempo máximo a esperar: ${maxTimeInMilliseconds} mS`);
        cy.log(`Tiempo tomado para recgargar página: ${timeCalculated} mS`);
        expect(timeCalculated).to.be.lessThan(maxTimeInMilliseconds);
      });
});

When("I navigate to the Website and intercept geolocation service", () => {
    //Interceptamos la petición que devuelve información de la locación y lo guardamos en countryCode
    cy.intercept("GET", "**/geo/location").as("geoLocationServiceRequest");
    cy.visit("/");
    cy.changeLanguageIfNeeded();
    cy.wait("@geoLocationServiceRequest").then((geoLocation) => {
        const countryCode = JSON.stringify(geoLocation.response.body.country).toLowerCase()
        cy.validateSchema(geoLocationSchema,geoLocation.response.body);
        cy.saveVariable('countryCode', countryCode.replace(/"/g, ''));
    })
});

Then("Validate that url contains the country code top-level domain -ccTLD-", () => {
    //Consumimos la variable countryCode y validamos que esté presente en la url
    cy.get("@countryCode").then((geoLocation) => {
        cy.log('Indicativo País: ' + geoLocation);
        cy.url().should('include', geoLocation);
    });
});

When("Select language {string}", (language) => {
    //Interactuamos con el select de idioma y seleccionamos idioma deseado.
    cy.get(landingPage.selectLanguage).select(language)
    cy.saveVariable('language', language);
});

Then("I validate that the language was changed correctly", () => {
    //Validamos el idioma de la página en el momento de la ejecución de la prueba.
    cy.get("@language").then((languageValue) => {
        if(languageValue==="Español"){
            cy.get('html').should('have.attr', 'lang', 'es');
            cy.waitFor(cy.get(landingPage.labelTitleAssert).should('be.visible').and('have.text','Películas y series ilimitadas y mucho más'));
        } else if (languageValue==="English") {
            cy.get('html').should('have.attr', 'lang', 'en');
            cy.get("@countryCode").then((countryCode) => {
                cy.waitFor(cy.get(landingPage.labelTitleAssert).should('be.visible').and('have.text','Unlimited movies, TV shows, and more'));
                cy.url().should('include', '/'+countryCode+'-en/');
            });
        }
    })
});

When("I do click in login button", () => {
    cy.get(landingPage.buttonLogin).click();
    cy.waitFor(cy.get(loginPage.labelTitleLogin).should('be.visible').and('have.text','Inicia sesión'));
});

Then("the page redirects to login page", () => {
    //Validamos redirección a la página de login
    cy.get("@countryCode").then((countryCode) => {
        cy.url().should('include', 'https://www.netflix.com/'+countryCode+'/login');
        cy.get(loginPage.inputEmail).should('be.visible');
        cy.get(loginPage.inputPass).should('be.visible');
        cy.get(loginPage.buttonLogin).should('be.enabled');
    });
});

When("I type a valid email", () => {
    //Generamos correo de prueba válido
    cy.generateRandomEmail(5, 'test.com').then((randomEmail) => {
        cy.log("randomEmail: " + randomEmail);
        cy.get(landingPage.inputEmail).first().scrollIntoView().type(randomEmail);
        cy.get(landingPage.buttonGetStarted).click();
      });
});

Then("the page redirects to sign up page", () => {
    //Validamos redirección a la página de sign up
    cy.waitFor(cy.get(signUpPage.labelTitle).should('be.visible'));
    cy.url().should('include', 'https://www.netflix.com/signup/registration');
    cy.get(signUpPage.labelTitle).should('have.text','Completa la configuración de tu cuenta');
    cy.get(signUpPage.labeSublTitle).should('have.text','Netflix está personalizado para ti. Crea una contraseña para comenzar a ver Netflix.');
    cy.get(signUpPage.buttonNext).should('be.enabled');
});

When("I type an invalid email", () => {
    //Generamos correo de prueba inválido
    cy.generateRandomEmail(5, '').then((randomEmail) => {
        cy.log("randomEmail: " + randomEmail);
        cy.get(landingPage.inputEmail).first().scrollIntoView().type(randomEmail);
        cy.get(landingPage.buttonGetStarted).click();
      });
});

Then("an invalid mail error message is displayed", () => {
    //Validamos mensaje de error cuando el email no es válido.
    cy.get(landingPage.labelErrorInvalidEmail).should('have.text','Escribe una dirección de email válida.');
});

When("I type a null email", () => {
    //Para enviar email vacío primero escribimos alguno y luego lo borramos, esta es la forma de replicar mensaje de error.
    cy.generateRandomEmail(5, '').then((randomEmail) => {
        cy.get(landingPage.inputEmail).first().scrollIntoView().type(randomEmail);
        cy.get(landingPage.inputEmail).first().scrollIntoView().clear();
        cy.get(landingPage.buttonGetStarted).click();
      });
});

Then("a null mail error message is displayed", () => {
    //Validamos mensaje de error cuando el email no es enviado
    cy.get(landingPage.labelErrorInvalidEmail).should('have.text','El email es obligatorio.');
});
