@HU_N5-001 @landing
Feature: N5 Challenge QA Automation SSr

  Background: I set url base
    Given I have the base url: "https://www.netflix.com/"

  @landing-devices @regression
  Scenario Outline: Navigate the Netflix page with differents devices
    When I maximize the screen in device: "<device>"
    Then Print and validate the title page
    And Print and validate the url
    Examples:
      | device  |
      | mobile  |
      | tablet  |
      | desktop |



  @landing-load-time 
  Scenario: Navigate the Netflix page and validation of page load time
    When I navigate to the Website and count the page load time 
    Then I validate that the page timeout is less than 5 seconds

  @landing-location
  Scenario: Navigate the Netflix page and validate location
    When I navigate to the Website and intercept geolocation service
    Then Print and validate the title page
    And Validate that url contains the country code top-level domain -ccTLD-

  @landing-languague @regression
  Scenario Outline: Navigate the Netflix page and validate language
    When I navigate to the Website and intercept geolocation service
    And Select language "<language>"
    Then I validate that the language was changed correctly
    Examples:
      | language |
      | Español  |
      | English  |
  


  @landing-login @regression
  Scenario: Navigate the Netflix page and validate the login button
    When I navigate to the Website and intercept geolocation service
    And I do click in login button
    Then the page redirects to login page

  @landing-login @landing-invalid-fields
  Scenario: Navigate the Netflix page and validate invalid email
      When I navigate to the Website and intercept geolocation service
      And I type an invalid email
      Then an invalid mail error message is displayed

  @landing-login @landing-invalid-fields
  Scenario: Navigate the Netflix page and validate null email
      When I navigate to the Website and intercept geolocation service
      And I type a null email
      Then a null mail error message is displayed

  @landing-sign-up @regression
  Scenario: Navigate the Netflix page and validate the sign up button
    When I navigate to the Website and intercept geolocation service
    And I type a valid email
    Then the page redirects to sign up page


