## Casos de prueba
Para esta prueba se implementó BDD para escribir los escenarios a probar, por lo que no se requiere especificar el paso a paso de los test cases como tradicionalmente se emplea. Los escenarios se encuentran en la carpeta ***cypress/e2e/*.feature***


## Patrón de diseño
* Page Object Model: se escogió inicialmente este patrón de diseño ya que la pruebas no representaban un nivel de complejidad alto y se cuenta con poco tiempo para llevar a cabo la implementación. Es un patrón que nos permite tener una estructura organizada y fácil de mantener. Ver rama: feature/netflixPOM.
* Screenplay: eventualmente se implementará en la rama: feature/netflixScreenplay.
Documentación:
    * https://snyk.io/advisor/npm-package/cypress-screenplay
    * https://github.com/AmazeeLabs/cypress-screenplay

## Tiempos de espera
### Esperas Implícitas
Las esperas implícitas son aquellas en las que se espera un tiempo determinado antes de realizar una acción o aserción. No se basan en condiciones específicas, solo esperan un tiempo fijo para que ocurran eventos. No se recomienda este uso ya que puede afectar en el rendimiento de la prueba ejecutando tiempos de espera innecesarios.
     
Ejemplo de uso:

* **cy.waits('time'):** Hay casos especificos en donde los elementos no son renderizados y cypress no los encuentra bajo ningun tipo de contexto. En esos casos se podría agregar unos segundos fijos para que espere la renderización del elemento. Solo se usa en casos muy puntuales ya que se considera una mala práctica. Nota: para este ejercicio no requerí usarlos. Documentación: https://docs.cypress.io/api/commands/wait
    ```
    cy.wait('2000'); //espera 2 segundos para pasar a la siguiente acción.
    ```
### Esperas Explícitas
Las esperas explícitas son aquellas en las que se espera hasta que se cumpla una condición específica en un elemento o un conjunto de elementos antes de continuar con la prueba. 
    
Ejemplo de uso:
    
* **cy.waits('@resourceAlias'):** espera a que se responda una solicitud específica antes de pasar al siguiente comando. Documentación: https://docs.cypress.io/api/commands/wait
    ```
    cy.wait('@getService').then((interception) => {
        // cy.get(...)
    });
    ```
* **cy.waitFor:** espera a que un elemento exista y esté visible. Documentación: https://www.npmjs.com/package/cypress-waitfor 
    ```
    cy.waitFor(cy.get(landingPage.labelTitleAssert).should('be.visible'));
    ```

## Ciclo de vida de la automatización.

### Planificación:

* Identificación de requerimientos: Comprende los requisitos funcionales y no funcionales de la aplicación que serán objeto de pruebas automatizadas, la selección de herramientas de automatización, la creación de estrategia de pruebas y la planificación de recursos: personal, tiempo y hardware, para la automatización de pruebas.
    Para este ejercicio se decide trabajar con cypress por su rapidez en implementación y poca curva de aprendizaje. Se implementarán las siguientes herramientas: 
    * BDD - Estrategia de desarrollo
    * NodeJS - Ambiente de servidor
    * NPM - Gestor de dependencias
    * Cucumber - Framework para automatizar pruebas BDD
    * Gherkin - Lenguaje Business Readable DSL (Lenguaje especifico de dominio legible por el negocio)

### Diseño de casos de prueba:

* Identificación de escenarios de prueba.
    Primero se identifican todos los posibles casos de pruebas candidatos a automatizar, se valoran los mismos en 3 grandes aspectos y se cuantifica un score de automatización.
    1. Valor
        * Importancia del caso de prueba (ALTA:3/MEDIA:2/BAJA:1)
        * Probabilidad de arreglo del caso de prueba en caso de error (ALTA:3/MEDIA:2/BAJA:1)
        * Diferenciación del caso de prueba (REPETITIVO:3/COMUN:2/DISTINTO:1)
    2. Riesgo
        * Impacto (en caso de error) a clientes (ALTO:3/MEDIO:2/BAJO:1)
        *  Probabilidad de uso de clientes (ALTA:3/MEDIA:2/BAJA:1)
    3. Costo
        *  Complejidad de escribir el script (DIFICIL:3/MEDIA:2/FACIL:1)
        *  Rapidez de escribir el script (ALTA:3/MEDIA:2/BAJA:1)
    
    Ejemplo de uso: 

    | TEST          | VALOR         | RIESGO        | COSTO         |    TOTAL     | 
    | ------------- | ------------- | ------------- | ------------- |------------- |
    | Login Netflix | 2+3+1=**2**   | 3+3=**3**     | 1+3=**2**     | **7**        | 
    
    Con este TOTAL se especifica por parte del QA un score que defina si un caso de prueba se automatiza o no.

* Creación de casos de prueba: Diseño de los casos de prueba detallados.  

    
### Implementación de automatización:

* Creación de scripts de prueba: Escritura de los scripts de automatización de pruebas utilizando la herramienta seleccionada.
* Revisión de código: Revisiones de código para garantizar que los scripts sean eficientes y cumplan con las pautas de codificación. Es una buena práctica ejecutar una revisión de nuestro código mediante herramientas de análiss de código estático como por ejemplo `sonar cloud`. Por tiempo acotado se decide no implemtar esta revisión.

### Ejecución de pruebas:

* Ejecución de los casos de prueba automatizados en el entorno de prueba.
* Generación de informes con los resultados de la ejecución.
* Registro de defectos: Si se encuentran problemas, se registran defectos en el sistema de seguimiento de defectos, ejemplo JIRA.

### Mantenimiento:

* Actualización de casos de prueba: mantenimiento de los casos de prueba automatizados para que reflejen los cambios en la aplicación.
* Depuración de scripts: Periódicamente se deben ejecutar pruebas de regresión y se resuelve cualquier problema que surja en los scripts de prueba.

### Cierre del ciclo:

* Revisión general del ciclo de vida, identificación de lecciones aprendidas y considerar ajustes para futuros ciclos.


### Reporte de estado de pruebas
Dado que al momento de ejecutar el comando para la interfaz grafica de Cypress **no** se genera un reporte html, hay que implementar una librería para dicho reporte, en este caso se decide utilizar la librería "multiple-cucumber-html-reporter" la cual es muy personalizable y facilita una posterior integración con pipelines en sistemas de CI/CD.

> :warning: **IMPORTANTE**:
> Cuando se ejecuta el comando para la interfaz grafica de Cypress **no** se genera un reporte html, por lo cual para tener un reporte después de la ejecucion de las pruebas se debe ejecutar el comando **npm run cypress:run**

### Buenas prácticas para automatizar

* **Pruebas 100% independientes.** Aislar las pruebas para evitar dependencias entre ellas, permitiendo la ejecución independiente y asegurando que los resultados sean predecibles.
    > Las pruebas 100% permiten implementar la ejecución paralela de pruebas para acelerar el proceso de ejecución y obtener resultados más rápidos.
* **Patrón diseño.** Su uso mejora el mantenimiento y reusabilidad de mis scripts.
* **Loggers.** En vez de usar el system out o el print de los distintos lenguajes se recomienda usar algún logger, el cual brinda flexibilidad de elegir que grado de loggin queremos en el output, INFO, ERRORS, DEBUG, entre otros. Son esenciales para garantizar una ejecución eficiente, un seguimiento adecuado de las pruebas y la resolución eficaz de problemas, se valoran mucho al momento de integración con pipelines.
    > En Cypress, no es necesario integrar una biblioteca de loggers como se haría un marco de prueba diferente (por ejemplo log4j para java-selenium). Cypress se enfoca en la ejecución de pruebas de extremo a extremo y proporciona sus propias capacidades de registro que son adecuadas para la mayoría de los casos de uso.
* **Validaciones robuztas.** Las validaciones, no pueden estar en la page class, deben estar desacopladas del modelado de la página, en la clase step definition. Otra buena práctica es agregar múltiples asserts en cada caso de prueba, esto para hacer nuestras pruebas mucho más robustas.
    > Debemos evitar en la medida de lo posible lo que Cypress denomina como los “tiny”
    tests donde solamente se tiene un solo assert por caso de prueba. No vamos a tener consecuencias en cuanto al
    performance de nuestro código porque las pruebas se ejecutan realmente rápido.
* **Parametrización de variables.** No usar valores harcodeados, de ser necesario manejar variables de entorno y para información sensible hacer uso de las librarys o variables de entorno de los pipelines.
    > Cypress permite trabajar con enfoque orientado a los datos (Dta driven testing), con el que podremos alimentar nuestros scripts con datos provenientes de archivos csv, excel, txt, json, entre otros.
    > Se recomienda utilizar archivos de configuración para gestionar entornos (por ejemplo, desarrollo, prueba, producción) y permitir una fácil adaptación de las pruebas a diferentes contextos.
* **Uso de selectores.** Deben se robuztos, que no sean tan susceptibles a errores si cambia algo en nuestra UI. Utilizar selectores únicos y estables al interactuar con elementos de la interfaz de usuario, evitando dependencias excesivas de la estructura del DOM. A modo personal prefiero los ids y los cssSelector ya que los navegadores están optimizados para evaluarlos rápidamente, la siguiente alternativa es el uso de xpaths relativos y construidos manualmente.
    > Se recomienda agregar un atributo único para interactuar con los elementos mas importantes del html, estos atributos serán exclusivamente para nuestras pruebas. Cypress recomienda la nomenclatura `data-cy`.
* **Timeouts.** Evitar los tiempos de espera implícitos (cy.waits). Utilizar esperas explícitas en lugar de esperas implícitas para garantizar que las acciones solo continúen cuando la aplicación está en un estado predefinido.
* **Gestión de Reportes.** Implementar una sólida generación de informes que sea comprensible y proporciona detalles precisos sobre los resultados de las pruebas.
* **Preparación ambiente de pruebas.** Si requerimos generación de datos de prueba deberíamos automatizar este proceso, la recomendación es hacer uso de APIs o mi opción preferida es inyectar querys directamente a las bases de datos de test (a la que deberíamos tener acceso). Adicional a la preparación del ambiente, es importante definir que acciones a ejecutar al finalizar la prueba, por ejemplo limpiar la data, generar reportes o restablecer sesiones. Para esto se emplean los hooks.
     > Puede ser una mala práctica generar mi data de prueba mediante la misma automatización E2E (mediante la UI), es preferible usar APIs o querys a las bases de datos.
* **Control de Versiones.** Utilizar sistemas de control de versiones para gestionar y rastrear los cambios en los scripts de prueba. Según la metodología Gitflow se recomienda tener como mínimo una rama master, una rama develop y diferentes ramas features para estabilizar las funcionalidades que vamos automatizando.
* **Manejo de Retries.** Configurar mecanismos de reintento (retries) para manejar de manera efectiva las fluctuaciones en la estabilidad de la aplicación y evitar falsos positivos. Para esta prueba se implemetó un feature de cypress denominado: `Cypress Flake Detection`.
* **Pruebas de regresión.** Se ejecutan las automatizaciones para correr las pruebas de regresión y asegurarte de que las nuevas modificaciones no afecten el funcionamiento existente. 
    >Una buena estrategia para implementar las pruebas de regresión, sería asignar tags a los escenarios acorde a su funcionalidad o agrupandolas para una posterior regresión de solo los tags seleccionados.

## CONCLUSIONES
* La página la encontré bastante estática, no habían animaciones o secciones dinámicas cuyo tiempo de espera afectaran las pruebas. Por esta razón se me hizo mucho mas fácil la programación de los casos de prueba.
* Diseñé un caso de prueba que medía el tiempo de carga de la página, en realidad es una aproximación para definir si posteriormente hay que hacer pruebas de performance, para estas últimas si se deben escoger herramientas adecuadas, como jmeter, K6 o gatling.
* En esta página los elementos se renderizan por completo lo que hace que las pruebas no sean complejas, existen sitios con casos especificos en donde los elementos no son renderizados por completo y cypress no los encuentra bajo ningun tipo de contexto, por lo que hay que hacer uso de estrategias no tan bien vistas como los tiempos de espera ramdoms (cy.waits). Por precaución se instaló la librería cypress-waitfor que lo que hace es esperar que X elemento exista y sea visible para proceder con los siguientes pasos del tests.
* Se observa que muchos elementos de la página tienen el atributo `data-uia` el cual nos brinda un selector específico que solo se usa para realizar pruebas. Esto es una buena práctica para cypress y disminuye la dificultad para desarrollar las pruebas.