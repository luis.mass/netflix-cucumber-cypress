const { defineConfig } = require("cypress");


module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
    env: {
      username: '',
      password: '',
      TAGS: "not @skip"
    },
    specFolder: 'cypress/e2e',
    specPattern: 'cypress/e2e/*.feature',
  },
  retries: {
    experimentalStrategy: 'detect-flake-but-always-fail',
    experimentalOptions: {
      maxRetries: 2,
      stopIfAnyPassed: true,
    },
    openMode: true,
    runMode: true,
  }
});